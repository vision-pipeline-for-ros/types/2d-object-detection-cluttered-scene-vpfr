#!/usr/bin/env python

"""
2d-object-detection
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
SPDX-License-Identifier: MIT
"""

import sys
from sensor_msgs.msg import Image
from vpfr2dobjectdetectionclutteredscene.msg import *
from vpfr import *


if __name__ == "__main__":

    if len(sys.argv) < 4:
        print(
            "usage: VpfrNode.py subscribechannel publishchannel AdditionalArgs"
        )
    else:
        VisionPipeline(
            "vpfr2dobjectdetectionclutteredscene",
            sys.argv[1],
            Image,
            sys.argv[2],
            object2DArray,
            sys.argv,
        )
